import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class DetailsList extends StatefulWidget {
  const DetailsList({Key? key}) : super(key: key);

  @override
  State<DetailsList> createState() => _DetailsListState();
}

class _DetailsListState extends State<DetailsList> {
  final Stream<QuerySnapshot> _myDetails =
      FirebaseFirestore.instance.collection("Details").snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _myDetails,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                child: SizedBox(child: 
                height: (MediaQuery.of(context).size.height),
                 width: MediaQuery.of(context).size.width,

                 child: ListView(children
                     snapshot.data!.docs.map((DocumentSnapshot documentSnapshot) => {
                       Map<String, dynamic> data = documentSnapshot.data() as Map<String, dynamic>;
                       return ListTitle(
                        title: Text(data['User_Name']),
                        subtitle: Text(data['Account_Type']),

                        );
                     }).toList(),
                 ),
                 ))
            ],
          );
        }else{
          return(Text("No data"));
        }
      },
    );
  }
}
