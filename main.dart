import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:web_app/addDetails.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
      apiKey: "AIzaSyCw1rXIDPk9jwhXSfz9CwTZI-vREtyTRKs",
      authDomain: "web-app-c4eb5.firebaseapp.com",
      projectId: "web-app-c4eb5",
      storageBucket: "web-app-c4eb5.appspot.com",
      messagingSenderId: "1088806891518",
      appId: "1:1088806891518:web:013bc0ea4142cda5d29aaf",
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Web App',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const MyHomePage(title: 'Money Account'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[AddDetails()],
        ),
      ),
    );
  }
}
