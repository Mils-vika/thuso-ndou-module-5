import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:web_app/detailsList.dart';

class AddDetails extends StatefulWidget {
  const AddDetails({Key? key}) : super(key: key);

  @override
  State<AddDetails> createState() => _AddDetailsState();
}

class _AddDetailsState extends State<AddDetails> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController branchController = TextEditingController();
    TextEditingController accountTypeController = TextEditingController();

    Future _addDetails() {
      final name = nameController.text;
      final branch = branchController.text;
      final accountType = accountTypeController.text;

      final ref = FirebaseFirestore.instance.collection("Details").doc();

      return ref
          .set({
            "User_Name": name,
            "Branch_Name": branch,
            "Account_Type": accountType,
            "doc_id": ref.id
          })
          .then((value) => log("collection added!!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20))),
                      hintText: "Enter Your Name")),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: branchController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular((20))),
                    hintText: "Enter Branch Name"),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: accountTypeController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular((20))),
                    hintText: "Enter Account Type"),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  _addDetails();
                },
                child: Text("Add Details"))
          ],
        ),
        DetailsList()
      ],
    );
  }
}
